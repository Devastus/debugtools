﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DebuggingTools
{
    public class DebugTools : MonoBehaviour
    {
        private static DebugTools _instance;
        public static DebugTools Instance { get
            {
                if(_instance == null)
                {
                    _instance = FindObjectOfType<DebugTools>();
                    if(_instance == null)
                    {
                        GameObject go = new GameObject("Debugger Tools");
                        _instance = go.AddComponent<DebugTools>();
                    }
                }
                return _instance;
            } }

        private DebugCLI console;
        private DebugMON monitor;

        public bool useConsole = true;
        public bool useMonitor = true;

#if DEBUG
        //void OnLevelWasLoaded()
        //{
        //    if (_instance == null)
        //    {
        //        _instance = FindObjectOfType<DebugTools>();
        //        if (_instance == null)
        //        {
        //            GameObject go = new GameObject("Debugger Tools");
        //            _instance = go.AddComponent<DebugTools>();
        //        }
        //    }
        //}
#endif

        void OnEnable()
        {
#if DEBUG
            if (useConsole)
            {
                if (console == null)
                {
                    GameObject go = new GameObject("DebugCLI");
                    go.transform.SetParent(transform);
                    console = go.AddComponent<DebugCLI>();
                }
                console.Enable();
            }

            if (useMonitor)
            {
                if (monitor == null)
                {
                    GameObject go = new GameObject("DebugMON");
                    go.transform.SetParent(transform);
                    monitor = go.AddComponent<DebugMON>();
                }
                monitor.Enable();
            }
#endif
        }

        void OnDisable()
        {
#if DEBUG
            if (useConsole) console.Disable();
            if (useMonitor) monitor.Disable();
#endif
        }

#if DEBUG
        void OnGUI()
        {

            if (useConsole) console.DrawGUI();
            if (useMonitor) monitor.DrawGUI();
        }
#endif

        void Update()
        {
#if DEBUG
            if (useConsole) { if (Input.GetKeyDown(console.ToggleKey)) console.Toggle(); }
            if (useMonitor) { if (Input.GetKeyDown(monitor.ToggleKey)) monitor.Toggle(); }
#endif
        }
    }
}


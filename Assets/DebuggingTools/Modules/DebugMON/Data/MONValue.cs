﻿namespace DebuggingTools
{
    [System.Serializable]
    public struct MONValue
    {
        private object _value;

        public MONValue(object value)
        {
            this._value = value;
        }

        public float Float()
        {
            if (_value.GetType() != typeof(float)) throw new System.Exception("MONValue.Float(): Data type mismatch!");
            return (float)_value;
        }

        public int Int()
        {
            if (_value.GetType() != typeof(int)) throw new System.Exception("MONValue.Int(): Data type mismatch!");
            return (int)_value;
        }

        public bool Bool()
        {
            if (_value.GetType() != typeof(bool)) throw new System.Exception("MONValue.Bool(): Data type mismatch!");
            return (bool)_value;
        }
    }
}
﻿using System.IO;
using UnityEngine;

namespace DebuggingTools
{
    [System.Serializable]
    public class MONSettings
    {
        private const string PATH = "/DebuggingTools/Modules/DebugMON/Resources/MONSettings.json";

        public Color titleColor;
        public Color monitorColor;
        //public Color fieldColor;

        public float monitorWidthPercentage;
        public float monitorHeightPercentage;
        public float opacity;
        public int fontSize;
        public bool bold;

        public MONSettings()
        {
            titleColor = new Color(0.1f, 0.1f, 0.1f, 1f);
            monitorColor = new Color(0f, 0f, 0f, 0.75f);
            //fieldColor = new Color(0f, 0f, 0f, 1f);
            monitorWidthPercentage = 0.275f;
            monitorHeightPercentage = 0.075f;
            opacity = 0.8f;
            fontSize = 10;
        }

        public void Serialize()
        {
            string filePath = Application.dataPath + PATH;
            string json = JsonUtility.ToJson(this, true);
            File.WriteAllText(filePath, json, System.Text.Encoding.Default);
        }

        public static MONSettings Deserialize()
        {
            string filePath = Application.dataPath + PATH;
            if (File.Exists(filePath))
            {
                string jsonString = File.ReadAllText(filePath);
                return JsonUtility.FromJson<MONSettings>(jsonString);
            }
            else
            {
                return new MONSettings();
            }
        }
    }
}
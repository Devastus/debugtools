﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DebuggingTools
{
    public class DebugMON : DebugToolsModule
    {
        private const string VERSION = "1.0";
        private const string LINE_END = "\n";
        private const string TITLE = "DebugMON";
        private const int FRAME_LIMIT = 250;
        private const float UPDATE_INTERVAL = 0.0833f; //12fps
        private const int MAX_REGISTERS = 10;

        private static Texture2D BGTEX;
        private static GUIStyle MONITOR_STYLE;
        private static GUIStyle TEXT_STYLE;
        //private static GUIStyle FIELD_STYLE;
        private static GUIStyle TITLE_STYLE;
        private static Font FONT;
        private static MONSettings SETTINGS;

        private Color _oldBackgroundColor;
        private Rect _monitorRect;
        //private Rect _singleMonitorRect;
        //private Rect _fieldRect;
        private Rect _titleRect;

        private List<MONTracker> monitors;

        private float timer = 0f;

        public override KeyCode ToggleKey { get { return KeyCode.F11; } }

        public override void Enable()
        {
            monitors = new List<MONTracker>();
            SETTINGS = MONSettings.Deserialize();
            BGTEX = Texture2D.whiteTexture;
            FONT = (Font)Resources.Load("Inconsolata");
            MONITOR_STYLE = new GUIStyle { normal = new GUIStyleState { background = BGTEX, textColor = Color.white }, alignment = TextAnchor.MiddleCenter, padding = new RectOffset(5, 5, 5, 5), fontSize = SETTINGS.fontSize, fontStyle = SETTINGS.bold ? FontStyle.Bold : FontStyle.Normal, font = FONT, wordWrap = false };
            TITLE_STYLE = new GUIStyle { normal = new GUIStyleState { background = BGTEX, textColor = Color.white }, alignment = TextAnchor.MiddleCenter, padding = new RectOffset(5, 5, 5, 5), fontSize = SETTINGS.fontSize, fontStyle = SETTINGS.bold ? FontStyle.Bold : FontStyle.Normal, font = FONT, wordWrap = false };
            base.Enable();
        }

        public override void Disable()
        {
            base.Disable();
        }

        public override void Toggle()
        {
            base.Toggle();
            if(_show == true)
            {
                UpdateGUIParameters();
            }
        }

        public override void DrawGUI()
        {
            if (_show)
            {
                base.DrawGUI();
                _oldBackgroundColor = GUI.backgroundColor;

                //Monitors
                int count = monitors.Count;
                GUI.backgroundColor = SETTINGS.monitorColor * SETTINGS.opacity;
                if(count > 0)
                {

                } else
                {
                    GUI.Box(_monitorRect, "No monitors active", MONITOR_STYLE);
                }

                //Title
                GUI.backgroundColor = SETTINGS.titleColor * SETTINGS.opacity;
                GUI.Box(_titleRect, TITLE, TITLE_STYLE);
                GUI.backgroundColor = _oldBackgroundColor;
            }
        }

        public override void Update()
        {
            if(timer >= UPDATE_INTERVAL)
            {
                int count = monitors.Count;
                for (int i = 0; i < count; i++)
                {
                    monitors[i].Update();
                }
                timer %= UPDATE_INTERVAL;
            }
            timer = timer + Time.deltaTime;
        }

        public void RegisterMonitor(object target, string propertyName)
        {
            var props = target.GetType().GetProperties();
            int length = props.Length;
            for (int i = 0; i < length; i++)
            {
                if (props[i].ToString().ToLower().Contains(propertyName))
                {
                    //Register as a new monitor

                    UpdateGUIParameters();
                    break;
                }
            }
        }

        public void UnregisterMonitor(string propertyName)
        {

        }

        private void UpdateGUIParameters()
        {
            float titleHeight = SETTINGS.fontSize * 1.5f;
            float monitorHeight = Screen.height * SETTINGS.monitorHeightPercentage;
            float monitorWidth = Screen.width * SETTINGS.monitorWidthPercentage;
            _titleRect = new Rect(0, 0, SETTINGS.fontSize * 8, titleHeight);
            _monitorRect = new Rect(0, titleHeight, monitorWidth, monitorHeight);
        }
    }
}
﻿using System.IO;
using UnityEngine;

namespace DebuggingTools
{
    [System.Serializable]
    public class CLISettings
    {
        private const string PATH = "/DebuggingTools/Modules/DebugCLI/Resources/CLISettings.json";
        [System.NonSerialized]
        public readonly string messageColorEnd = "</color>";

        public Color titleColor;
        public Color logColor;
        public Color fieldColor;
        public float consoleHeightPercentage;
        public float opacity;
        public int fontSize;
        public bool bold;

        public string messageBaseColor;
        public string messageNotificationColor;
        public string messageWarningColor;
        public string messageAffirmationColor;
        public string messageCommandColor;
        public string messageFunctionColor;
        public string messageParameterColor;
        public string messageNoneColor;

        public CLISettings()
        {
            titleColor = new Color(0.1f, 0.1f, 0.1f, 1f);
            logColor = new Color(0f, 0f, 0f, 0.8f);
            fieldColor = new Color(0f, 0f, 0f, 1f);
            consoleHeightPercentage = 0.4f;
            opacity = 0.8f;
            fontSize = 12;
            bold = false;
            messageBaseColor = "<color=#FFFFFF>";
            messageNotificationColor = "<color=#FFFF00>";
            messageWarningColor = "<color=#FF3333>";
            messageAffirmationColor = "<color=#33FF33>";
            messageCommandColor = "<color=#99EEFF>";
            messageFunctionColor = "<color=#77BBFF>";
            messageParameterColor = "<color=#BBEE44>";
            messageNoneColor = "<color=#888888>";
            messageColorEnd = "</color>";
        }

        public void Serialize()
        {
            string filePath = Application.dataPath + PATH;
            string json = JsonUtility.ToJson(this, true);
            File.WriteAllText(filePath, json, System.Text.Encoding.Default);
        }

        public static CLISettings Deserialize()
        {
            string filePath = Application.dataPath + PATH;
            if (File.Exists(filePath))
            {
                string jsonString = File.ReadAllText(filePath);
                return JsonUtility.FromJson<CLISettings>(jsonString);
            }
            else
            {
                return new CLISettings();
            }
        }
    }
}
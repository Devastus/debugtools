﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DebuggingTools
{
    [System.Serializable]
    public class CLICommand
    {
        public Dictionary<string, CmdFunc> methods;

        public CLICommand()
        {
            methods = new Dictionary<string, CmdFunc>();
        }

        public void Add(string key, System.Reflection.MethodInfo method, System.Type issuerType = null, string description = "")
        {
            methods.Add(key, new CmdFunc(method, issuerType, description));
        }

        public void Remove(string key)
        {
            methods.Remove(key);
        }

        #region Invoke Methods

        public bool InvokeFunction(string[] args)
        {
            CmdFunc func = default(CmdFunc);

            //If we can find a function with this argument, it's an identifier
            if (args.Length > 1 && TryGetFunction(args[1], out func))
            {
                int inputParametersLength = args.Length - 2;
                object[] parameters = null;
                System.Reflection.ParameterInfo[] paramInfo = func.method.GetParameters();
                bool succesful = ParseAllParameters(args, inputParametersLength, paramInfo, out parameters);
                if (succesful)
                {
                    if (func.issuerType == null)
                    {
                        func.method.Invoke(null, parameters);
                    }
                    else
                    {
                        func.method.Invoke(Object.FindObjectOfType(func.issuerType), parameters);
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }

            //Else, if we can find a function with an empty string, it's most likely a parameter (or null)
            else if (TryGetFunction(string.Empty, out func))
            {
                int inputParametersLength = args.Length - 1;
                object[] parameters = null;
                System.Reflection.ParameterInfo[] paramInfo = func.method.GetParameters();
                bool succesful = ParseAllParameters(args, inputParametersLength, paramInfo, out parameters);
                if (succesful)
                {
                    if (func.issuerType == null)
                    {
                        func.method.Invoke(null, parameters);
                    }
                    else
                    {
                        func.method.Invoke(Object.FindObjectOfType(func.issuerType), parameters);
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }

            if (args.Length > 1)
            {
                DebugCLI.OnMessage(DebugCLI.GetColoredText(args[0], DebugCLI.TextColorType.Command) + ": Function '" + DebugCLI.GetColoredText(args[1], DebugCLI.TextColorType.Function) + "' is invalid", DebugCLI.MessageType.Warning);
            }
            else
            {
                DebugCLI.OnMessage(DebugCLI.GetColoredText(args[0], DebugCLI.TextColorType.Command) + ": Function '" + DebugCLI.GetColoredText("(none)", DebugCLI.TextColorType.None) + "' is invalid", DebugCLI.MessageType.Warning);
            }
            return false;
        }

        public bool TryGetFunction(string key, out CmdFunc func)
        {
            func = default(CmdFunc);
            bool find = methods.TryGetValue(key, out func);
            return find;
        }

        private bool ParseAllParameters(string[] args, int inputParametersLength, System.Reflection.ParameterInfo[] paramInfo, out object[] parameters)
        {
            int length = paramInfo.Length;
            if (inputParametersLength != length)
            {
                int optionalsLength = 0;
                for (int i = 0; i < length; i++)
                {
                    if (paramInfo[i].IsOptional) optionalsLength++;
                }
                if (inputParametersLength < length - optionalsLength)
                {
                    parameters = null;
                    string issuer = DebugCLI.GetColoredText(args[0], DebugCLI.TextColorType.Command) + (args.Length - inputParametersLength == 2 ? " -> " + DebugCLI.GetColoredText(args[1], DebugCLI.TextColorType.Function) : string.Empty);
                    string error = issuer + ": Invalid number of parameters given! Expected parameters: ";
                    for (int i = 0; i < length; i++)
                    {
                        error += DebugCLI.GetColoredText("(" + paramInfo[i].Name + ":" + DebugCLI.GetStrippedParameterName(paramInfo[i].ParameterType.ToString()) + ")", DebugCLI.TextColorType.Parameter) + (i < length - 1 ? ", " : string.Empty);
                    }
                    DebugCLI.OnMessage(error, DebugCLI.MessageType.Warning);
                    return false;
                }
            }
            parameters = new object[length];
            int diff = args.Length - inputParametersLength;
            for (int i = args.Length - 1; i > diff - 1; i--)
            {
                if (!TryParseParameter(args[i], paramInfo[i - diff].ParameterType, out parameters[i - diff]))
                {
                    string issuer = DebugCLI.GetColoredText(args[0], DebugCLI.TextColorType.Command) + (args.Length - inputParametersLength == 2 ? " -> " + DebugCLI.GetColoredText(args[1], DebugCLI.TextColorType.Function) : string.Empty);
                    string error = issuer + ": Parameter '" + DebugCLI.GetColoredText(args[i], DebugCLI.TextColorType.Parameter) + "' is invalid! Expected parameter: " + DebugCLI.GetColoredText("(" + paramInfo[i - diff].Name + ":" + DebugCLI.GetStrippedParameterName(paramInfo[i - diff].ParameterType.ToString()) + ")", DebugCLI.TextColorType.Parameter);
                    DebugCLI.OnMessage(error, DebugCLI.MessageType.Warning);
                    return false;
                }
            }
            return true;
        }

        private bool TryParseParameter(string argument, System.Type expectedType, out object parameter)
        {
            parameter = null;
            if (expectedType == typeof(string))
            {
                parameter = argument;
                return true;
            }
            else
            {
                if (expectedType == typeof(int))
                {
                    int value;
                    bool parse = int.TryParse(argument, out value);
                    if (parse) parameter = value; else parameter = null;
                    return parse;
                }
                else if (expectedType == typeof(float))
                {
                    float value;
                    bool parse = float.TryParse(argument, out value);
                    if (parse) parameter = value; else parameter = null;
                    return parse;
                }
                else if (expectedType == typeof(bool))
                {
                    bool value;
                    bool parse = bool.TryParse(argument, out value);
                    if (parse) parameter = value; else parameter = null;
                    return parse;
                }
                else if (expectedType == typeof(Vector2))
                {
                    string[] values = argument.Split(',');
                    if (values.Length != 2) return false;
                    float val1, val2;
                    bool parse1 = float.TryParse(values[0], out val1);
                    bool parse2 = float.TryParse(values[1], out val2);
                    if (parse1 && parse2) parameter = new Vector2(val1, val2); else parameter = null;
                    return parse1 && parse2;
                }
                else if (expectedType == typeof(Vector3))
                {
                    string[] values = argument.Split(',');
                    if (values.Length != 3) return false;
                    float val1, val2, val3;
                    bool parse1 = float.TryParse(values[0], out val1);
                    bool parse2 = float.TryParse(values[1], out val2);
                    bool parse3 = float.TryParse(values[2], out val3);
                    if (parse1 && parse2 && parse3) parameter = new Vector3(val1, val2, val3); else parameter = null;
                    return parse1 && parse2 && parse3;
                }
                else if (expectedType == typeof(Color) || expectedType == typeof(Vector4))
                {
                    string[] values = argument.Split(',');
                    if (values.Length != 4) return false;
                    float val1, val2, val3, val4;
                    bool parse1 = float.TryParse(values[0], out val1);
                    bool parse2 = float.TryParse(values[1], out val2);
                    bool parse3 = float.TryParse(values[2], out val3);
                    bool parse4 = float.TryParse(values[3], out val4);
                    if (parse1 && parse2 && parse3 && parse4)
                    {
                        if (expectedType == typeof(Color))
                            parameter = new Color(val1, val2, val3, val4);
                        else
                            parameter = new Vector4(val1, val2, val3, val4);
                    }
                    else parameter = null;
                    return parse1 && parse2 && parse3 && parse4;
                }
            }
            return false;
        }
        #endregion
    }

    [System.Serializable]
    public struct CmdFunc
    {
        public System.Reflection.MethodInfo method;
        public System.Type issuerType;
        public string description;

        public CmdFunc(System.Reflection.MethodInfo method, System.Type issuerType, string description)
        {
            this.method = method;
            this.issuerType = issuerType;
            this.description = description;
        }
    }
}
﻿using System;

[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
public class ConsoleCommandAttribute : Attribute {

    const string EMPTY = "";

    public string cmdName;
    public string funcName;
    public string description;

	public ConsoleCommandAttribute(string cmdName, string funcName = EMPTY, string description = null)
    {
        this.cmdName = cmdName;
        this.funcName = funcName;
        this.description = description;
    }
}

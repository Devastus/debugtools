﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace DebuggingTools
{
    public class DebugCLI : DebugToolsModule
    {
        public enum TextColorType
        {
            Base,
            Notification,
            Warning,
            Affirmation,
            Command,
            Function,
            Parameter,
            None
        }

        public enum MessageType
        {
            Basic,
            Notification,
            Warning,
            Affirmation
        }

        public static System.Action<string, MessageType> OnMessage;

        private const string VERSION = "1.0";
        private const string LINE_END = "\n";
        private const string TITLE = "DebugCLI";
        private static int MAX_MESSAGE_COUNT = 20;
        private const int MAX_HISTORY_COUNT = 8;

        private static Texture2D BGTEX;
        private static GUIStyle LOGBG_STYLE;
        private static GUIStyle LOGTEXT_STYLE;
        private static GUIStyle FIELD_STYLE;
        private static GUIStyle TITLE_STYLE;
        private static Font FONT;
        private static CLISettings SETTINGS;

        private static Dictionary<string, CLICommand> _commands;
        private static List<string> _history = new List<string>();
        private static List<string> _backlog = new List<string>();
        private static int _activeHistoryIndex = -1;

        public override KeyCode ToggleKey { get { return KeyCode.F12; } }
        
        private float _prevTimeScale = 1f;
        private Color _oldBackgroundColor;

        private Rect _logRect;
        private Rect _logTextRect;
        private Rect _fieldRect;
        private Rect _titleRect;
        private Rect _targetRect;
        private string _commandString;
        private string _logString;
        private object _target;

        /// <summary>
        /// Enable and initialize the DebugCLI. The Console will begin logging messages directed at it.
        /// </summary>
        public override void Enable()
        {
            base.Enable();
            OnMessage = WriteToConsole;
            SETTINGS = CLISettings.Deserialize();
            _backlog = new List<string>(MAX_MESSAGE_COUNT);
            _history = new List<string>(MAX_HISTORY_COUNT);
            BGTEX = Texture2D.whiteTexture;
            FONT = (Font)Resources.Load("Inconsolata");
            LOGBG_STYLE = new GUIStyle { normal = new GUIStyleState { background = BGTEX, textColor = Color.white }, alignment = TextAnchor.UpperLeft, padding = new RectOffset(5, 5, 5, 5), fontSize = SETTINGS.fontSize, fontStyle = SETTINGS.bold ? FontStyle.Bold : FontStyle.Normal, font = FONT, wordWrap = true };
            LOGTEXT_STYLE = new GUIStyle { normal = new GUIStyleState { background = null, textColor = Color.white }, alignment = TextAnchor.UpperLeft, padding = new RectOffset(5, 5, 5, 5), fontSize = SETTINGS.fontSize, fontStyle = SETTINGS.bold ? FontStyle.Bold : FontStyle.Normal, font = FONT, wordWrap = true };
            FIELD_STYLE = new GUIStyle { normal = new GUIStyleState { background = BGTEX, textColor = Color.white }, alignment = TextAnchor.MiddleLeft, padding = new RectOffset(5, 5, 0, 0), fontSize = SETTINGS.fontSize, fontStyle = SETTINGS.bold ? FontStyle.Bold : FontStyle.Normal, font = FONT, wordWrap = false };
            TITLE_STYLE = new GUIStyle { normal = new GUIStyleState { background = BGTEX, textColor = Color.white }, alignment = TextAnchor.MiddleCenter, padding = new RectOffset(5, 5, 5, 5), fontSize = SETTINGS.fontSize, fontStyle = SETTINGS.bold ? FontStyle.Bold : FontStyle.Normal, font = FONT, wordWrap = false };
            ScanAssemblyForCommands();
            WriteToConsole("DebugCLI " + VERSION + " -- type 'help' to see all commands.", MessageType.Affirmation);
        }

        /// <summary>
        /// Disable the DebugCLI. The Console will no longer log any messages.
        /// </summary>
        public override void Disable()
        {
            base.Disable();
            OnMessage = null;
            _backlog.Clear();
            _history.Clear();
        }

        /// <summary>
        /// Toggle whether the CLI should be visually shown. This has no effect on Console logging.
        /// </summary>
        public override void Toggle()
        {
            base.Toggle();
            if(_show == true)
            {
                _prevTimeScale = Time.timeScale;
                Time.timeScale = 0f;
                UpdateGUIParameters();
            } else
            {
                Time.timeScale = _prevTimeScale;
            }
        }

        #region GUI Methods

        /// <summary>
        /// Draw the CLI GUI.
        /// </summary>
        public override void DrawGUI()
        {
            if (_show)
            {
                base.DrawGUI();
                Event e = Event.current;
                HandleEvents(e);
                _oldBackgroundColor = GUI.backgroundColor;

                //Log
                GUI.backgroundColor = SETTINGS.logColor * SETTINGS.opacity;
                _logTextRect = GUILayoutUtility.GetRect(new GUIContent(_logString), LOGTEXT_STYLE);
                _logTextRect.y = (_logRect.height + SETTINGS.fontSize + (SETTINGS.fontSize * 0.375f)) - _logTextRect.height;
                GUI.Box(_logRect, GUIContent.none, LOGBG_STYLE);
                GUI.BeginGroup(_logRect);
                GUI.Label(_logTextRect, _logString, LOGTEXT_STYLE);
                GUI.EndGroup();

                //Input field
                GUI.backgroundColor = SETTINGS.fieldColor * SETTINGS.opacity;
                GUI.SetNextControlName("CommandField");
                GUI.Label(new Rect(_fieldRect.x, _fieldRect.y, SETTINGS.fontSize, _fieldRect.height), "> ", FIELD_STYLE);
                _commandString = GUI.TextField(new Rect(_fieldRect.x + SETTINGS.fontSize, _fieldRect.y, _fieldRect.width - SETTINGS.fontSize, _fieldRect.height), _commandString, FIELD_STYLE);
                GUI.FocusControl("CommandField");

                //Title
                GUI.backgroundColor = SETTINGS.titleColor * SETTINGS.opacity;
                GUI.Box(_titleRect, TITLE, TITLE_STYLE);
                GUI.backgroundColor = _oldBackgroundColor;
            }
        }

        private void HandleEvents(Event e)
        {
            if(e.type == EventType.KeyDown)
            {
                //Press Enter
                if(e.keyCode == KeyCode.Return)
                {
                    ProcessCommand();
                }
                else if(e.keyCode == ToggleKey)
                {
                    Toggle();
                }
                else if (e.keyCode == KeyCode.UpArrow)
                {
                    //Go back in history
                    if(_activeHistoryIndex+1 < _history.Count)
                    {
                        _activeHistoryIndex++;
                        _commandString = _history[_activeHistoryIndex];
                    }
                }
                else if (e.keyCode == KeyCode.DownArrow)
                {
                    //Go forth in history (or clear the input field if can't go further)
                    if(_activeHistoryIndex-1 >= 0)
                    {
                        _activeHistoryIndex--;
                        _commandString = _history[_activeHistoryIndex];
                    } else
                    {
                        _activeHistoryIndex = -1;
                        _commandString = string.Empty;
                    }
                }
            }
        }

        private void UpdateGUIParameters()
        {
            LOGBG_STYLE.fontSize = SETTINGS.fontSize;
            LOGTEXT_STYLE.fontSize = SETTINGS.fontSize;
            FIELD_STYLE.fontSize = SETTINGS.fontSize;
            TITLE_STYLE.fontSize = SETTINGS.fontSize;
            
            float logHeight = Screen.height * SETTINGS.consoleHeightPercentage;
            float fieldHeight = SETTINGS.fontSize + (SETTINGS.fontSize * 0.75f);
            MAX_MESSAGE_COUNT = (int)(logHeight / SETTINGS.fontSize);

            _logRect = new Rect(0, Screen.height - logHeight, Screen.width, logHeight - fieldHeight);
            _fieldRect = new Rect(0, Screen.height - fieldHeight, Screen.width, fieldHeight);
            _titleRect = new Rect(0, Screen.height - logHeight, SETTINGS.fontSize * 8, SETTINGS.fontSize * 1.5f);

            int length = _backlog.Count;
            if (length > MAX_MESSAGE_COUNT)
            {
                int diff = length - MAX_MESSAGE_COUNT;
                _backlog.RemoveRange((length-1) - diff, diff);
            }
            _logString = string.Empty;
            length = _backlog.Count;
            for (int i = length - 1; i >= 0; i--)
            {
                _logString += _backlog[i];
            }
        }

        /// <summary>
        /// An event function that writes a message to the Console Log.
        /// </summary>
        /// <param name="text">The message.</param>
        /// <param name="messageType">Type of the message.</param>
        private void WriteToConsole(string text, MessageType messageType)
        {
            string endResult = string.Empty;
            switch (messageType)
            {
                case MessageType.Basic: endResult = GetColoredText(text + LINE_END, TextColorType.Base); break;
                case MessageType.Notification: endResult = GetColoredText(text + LINE_END, TextColorType.Notification); break;
                case MessageType.Warning: endResult = GetColoredText(text + LINE_END, TextColorType.Warning); break;
                case MessageType.Affirmation: endResult = GetColoredText(text + LINE_END, TextColorType.Affirmation); break;
            }
            _backlog.Insert(0, endResult);
            if(_backlog.Count > MAX_MESSAGE_COUNT)
            {
                _backlog.RemoveAt(MAX_MESSAGE_COUNT);
            }
            UpdateGUIParameters();
        }

        public static string GetColoredText(string input, TextColorType type)
        {
            switch (type)
            {
                default: return input;
                case TextColorType.Base:
                    return SETTINGS.messageBaseColor + input + SETTINGS.messageColorEnd;
                case TextColorType.Notification:
                    return SETTINGS.messageNotificationColor + input + SETTINGS.messageColorEnd;
                case TextColorType.Warning:
                    return SETTINGS.messageWarningColor + input + SETTINGS.messageColorEnd;
                case TextColorType.Affirmation:
                    return SETTINGS.messageAffirmationColor + input + SETTINGS.messageColorEnd;
                case TextColorType.Command:
                    return SETTINGS.messageCommandColor + input + SETTINGS.messageColorEnd;
                case TextColorType.Function:
                    return SETTINGS.messageFunctionColor + input + SETTINGS.messageColorEnd;
                case TextColorType.Parameter:
                    return SETTINGS.messageParameterColor + input + SETTINGS.messageColorEnd;
                case TextColorType.None:
                    return SETTINGS.messageNoneColor + input + SETTINGS.messageColorEnd;
            }
        }

        public static string GetStrippedParameterName(string input)
        {
            return input.Split('.')[1];
        }

        #endregion

        #region Command Methods

        private void ProcessCommand()
        {
            _commandString = _commandString.ToLower();
            _history.Insert(0, _commandString);
            if (_history.Count > MAX_HISTORY_COUNT) _history.RemoveAt(_history.Count - 1);
            WriteToConsole("> " + _commandString, MessageType.Basic);
            InvokeCommand(_commandString.Split(' '));
            _commandString = string.Empty;
        }

        private bool InvokeCommand(params string[] args)
        {
            int length = args.Length;
            if (length > 0)
            {
                CLICommand cmd;
                if (_commands.TryGetValue(args[0], out cmd))
                {
                    return cmd.InvokeFunction(args);
                }
                else
                {
                    string error = "Command '" + DebugCLI.GetColoredText(args[0], DebugCLI.TextColorType.Command) + "' is invalid!";
                    DebugCLI.OnMessage(error, DebugCLI.MessageType.Warning);
                }
            }
            return false;
        }

        private void ScanAssemblyForCommands()
        {
            _commands = new Dictionary<string, CLICommand>();
            System.Type[] types = System.Reflection.Assembly.GetExecutingAssembly().GetTypes();
            int tLength = types.Length;
            for (int t = 0; t < tLength; t++)
            {
                System.Reflection.MethodInfo[] methods = types[t].GetMethods(System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Static);
                int mLength = methods.Length;
                for (int m = 0; m < mLength; m++)
                {
                    object[] attr = methods[m].GetCustomAttributes(typeof(ConsoleCommandAttribute), false);
                    if (attr.Length > 0)
                    {
                        ConsoleCommandAttribute cmdAttr = (ConsoleCommandAttribute)attr[0];
                        CLICommand cmd;
                        bool find = _commands.TryGetValue(cmdAttr.cmdName, out cmd);
                        if (!find)
                        {
                            cmd = new CLICommand();
                            _commands.Add(cmdAttr.cmdName, cmd);
                        }
                        cmd.Add(cmdAttr.funcName, methods[m], !methods[m].IsStatic ? types[t] : null, cmdAttr.description);
                    }
                }
            }
        }

        #endregion

        #region Utility Console Commands

        [ConsoleCommand("console", funcName = "height", description = "Changes the height percentage of the console log (0-1)")]
        public void ChangeHeight(float height)
        {
            SETTINGS.consoleHeightPercentage = Mathf.Clamp01(height);
            UpdateGUIParameters();
            SETTINGS.Serialize();
        }

        [ConsoleCommand("console", funcName = "fontsize", description = "Changes the font size of the console")]
        public void ChangeFontSize(int size)
        {
            SETTINGS.fontSize = size;
            UpdateGUIParameters();
            SETTINGS.Serialize();
        }

        [ConsoleCommand("console", funcName = "bold", description = "Changes whether or not the font is bold")]
        public void ChangeFontBold(bool bold)
        {
            SETTINGS.bold = bold;
            LOGBG_STYLE.fontStyle = SETTINGS.bold ? FontStyle.Bold : FontStyle.Normal;
            LOGTEXT_STYLE.fontStyle = SETTINGS.bold ? FontStyle.Bold : FontStyle.Normal;
            FIELD_STYLE.fontStyle = SETTINGS.bold ? FontStyle.Bold : FontStyle.Normal;
            TITLE_STYLE.fontStyle = SETTINGS.bold ? FontStyle.Bold : FontStyle.Normal;
            SETTINGS.Serialize();
        }

        [ConsoleCommand("console", funcName = "opacity", description = "Changes the opacity of the console background (0-1)")]
        public void ChangeOpacity(float value)
        {
            SETTINGS.opacity = Mathf.Clamp01(value);
            UpdateGUIParameters();
            SETTINGS.Serialize();
        }

        [ConsoleCommand("clear", description = "Clears the console log")]
        public void Clear()
        {
            _backlog.Clear();
            UpdateGUIParameters();
            WriteToConsole("DebugCLI " + VERSION + " -- type 'help' to see all commands.", MessageType.Affirmation);
        }

        [ConsoleCommand("clear", funcName = "history", description = "Clears the console input history")]
        public void ClearHistory()
        {
            _history.Clear();
            UpdateGUIParameters();
        }

        [ConsoleCommand("clear", funcName = "all", description = "Clears everything, essentially resetting the console")]
        public void ClearAll()
        {
            _backlog.Clear();
            _history.Clear();
            UpdateGUIParameters();
            WriteToConsole("DebugCLI " + VERSION + " -- type 'help' to see all commands.", MessageType.Affirmation);
        }

        [ConsoleCommand("console", funcName = "reload", description = "Reloads console settings from file")]
        public void RefreshSettings()
        {
            SETTINGS = CLISettings.Deserialize();
            LOGBG_STYLE = new GUIStyle { normal = new GUIStyleState { background = BGTEX, textColor = Color.white }, alignment = TextAnchor.UpperLeft, padding = new RectOffset(5, 5, 5, 5), fontSize = SETTINGS.fontSize, fontStyle = SETTINGS.bold ? FontStyle.Bold : FontStyle.Normal, font = FONT, wordWrap = true };
            LOGTEXT_STYLE = new GUIStyle { normal = new GUIStyleState { background = null, textColor = Color.white }, alignment = TextAnchor.UpperLeft, padding = new RectOffset(5, 5, 5, 5), fontSize = SETTINGS.fontSize, fontStyle = SETTINGS.bold ? FontStyle.Bold : FontStyle.Normal, font = FONT, wordWrap = true };
            FIELD_STYLE = new GUIStyle { normal = new GUIStyleState { background = BGTEX, textColor = Color.white }, alignment = TextAnchor.MiddleLeft, padding = new RectOffset(5, 5, 0, 0), fontSize = SETTINGS.fontSize, fontStyle = SETTINGS.bold ? FontStyle.Bold : FontStyle.Normal, font = FONT, wordWrap = false };
            TITLE_STYLE = new GUIStyle { normal = new GUIStyleState { background = BGTEX, textColor = Color.white }, alignment = TextAnchor.MiddleCenter, padding = new RectOffset(5, 5, 5, 5), fontSize = SETTINGS.fontSize, fontStyle = SETTINGS.bold ? FontStyle.Bold : FontStyle.Normal, font = FONT, wordWrap = false };
            UpdateGUIParameters();
            DebugCLI.OnMessage("Console settings reloaded.", MessageType.Affirmation);
        }

        [ConsoleCommand("help", description = "Lists all commands, or command functions and their descriptions based on names given")]
        public static void Help(string cmdName = null, string funcName = null)
        {
            if(cmdName == null && funcName == null)
            {
                DebugCLI.OnMessage("help: all commands", MessageType.Notification);
                foreach (KeyValuePair<string, CLICommand> pair in _commands)
                {
                    DebugCLI.OnMessage(GetColoredText(pair.Key, TextColorType.Command), MessageType.Basic);
                }
            } else if (funcName == null)
            {
                CLICommand cmd;
                if (_commands.TryGetValue(cmdName, out cmd))
                {
                    DebugCLI.OnMessage("help: command '" + DebugCLI.GetColoredText(cmdName, TextColorType.Command) + "' functions", MessageType.Notification);
                    foreach (KeyValuePair<string, CmdFunc> pair in cmd.methods)
                    {
                        string parameterInfo = string.Empty;
                        System.Reflection.ParameterInfo[] paramInfo = pair.Value.method.GetParameters();
                        int length = paramInfo.Length;
                        for (int i = 0; i < length; i++)
                        {
                            parameterInfo += GetColoredText("(" + paramInfo[i].Name + ":" + GetStrippedParameterName(paramInfo[i].ParameterType.ToString()) + ")", TextColorType.Parameter) + (i < (length - 1) ? "," : string.Empty);
                        }
                        string key = pair.Key != string.Empty ? GetColoredText(pair.Key+" ", TextColorType.Function) : GetColoredText("(none) ", TextColorType.None);
                        bool isDescription = pair.Value.description != null && pair.Value.description != string.Empty;
                        DebugCLI.OnMessage(key + parameterInfo + (isDescription ? " - " + pair.Value.description : string.Empty), MessageType.Basic);
                    }
                }
            } else if (cmdName != null && funcName != null)
            {
                CLICommand cmd;
                if (_commands.TryGetValue(cmdName, out cmd))
                {
                    CmdFunc func;
                    if (cmd.TryGetFunction(funcName, out func))
                    {
                        string parameterInfo = string.Empty;
                        System.Reflection.ParameterInfo[] paramInfo = func.method.GetParameters();
                        int length = paramInfo.Length;
                        for (int i = 0; i < length; i++)
                        {
                            parameterInfo += GetColoredText("(" + paramInfo[i].Name + ":" + GetStrippedParameterName(paramInfo[i].ParameterType.ToString()) + ")", TextColorType.Parameter) + (i < (length - 1) ? "," : string.Empty);
                        }
                        bool isDescription = func.description != null && func.description != string.Empty;
                        DebugCLI.OnMessage("help: command '" + GetColoredText(cmdName, TextColorType.Command) + "' -> function '" + GetColoredText(funcName, TextColorType.Function) + "'" + parameterInfo, MessageType.Notification);
                        if (isDescription) DebugCLI.OnMessage(func.description, MessageType.Basic);
                    }
                }
            }
        }

        [ConsoleCommand("help", funcName = "types", description = "All interpreted types and examples on how to write them.")]
        public static void HelpTypes()
        {
            DebugCLI.OnMessage("help: types", MessageType.Notification);
            DebugCLI.OnMessage(GetColoredText(GetStrippedParameterName(typeof(string).ToString()), TextColorType.Parameter) + " - example: HelloWorld!", MessageType.Basic);
            DebugCLI.OnMessage(GetColoredText(GetStrippedParameterName(typeof(int).ToString()), TextColorType.Parameter) + " - example: 1", MessageType.Basic);
            DebugCLI.OnMessage(GetColoredText(GetStrippedParameterName(typeof(float).ToString()), TextColorType.Parameter) + " - example: 1.0", MessageType.Basic);
            DebugCLI.OnMessage(GetColoredText(GetStrippedParameterName(typeof(bool).ToString()), TextColorType.Parameter) + " - example: true/false", MessageType.Basic);
            DebugCLI.OnMessage(GetColoredText(GetStrippedParameterName(typeof(Vector2).ToString()), TextColorType.Parameter) + " - example: 1.0,1.0", MessageType.Basic);
            DebugCLI.OnMessage(GetColoredText(GetStrippedParameterName(typeof(Vector3).ToString()), TextColorType.Parameter) + " - example: 1.0,1.0,1.0", MessageType.Basic);
            DebugCLI.OnMessage(GetColoredText(GetStrippedParameterName(typeof(Vector4).ToString()), TextColorType.Parameter) + " - example: 1.0,1.0,1.0,1.0", MessageType.Basic);
            DebugCLI.OnMessage(GetColoredText(GetStrippedParameterName(typeof(Color).ToString()), TextColorType.Parameter) + " - example: 0.5,0.5,0.5,0.5", MessageType.Basic);
        }

        [ConsoleCommand("spawn", description = "Spawns a new object from the hierarchy or the Resources folder into the scene.")]
        public static void Spawn(string name, Vector3 position = default(Vector3), Vector3 rotation = default(Vector3))
        {
            GameObject obj = GameObject.Find(name);
            if(obj == null)
            {
                obj = GameObject.Find(name + "(Clone)");
                if(obj == null)
                {
                    obj = (GameObject)Resources.Load(name, typeof(GameObject));
                    if(obj == null)
                    {
                        string[] files = Directory.GetFiles(Application.dataPath + "/Resources/", name+"*", SearchOption.AllDirectories);
                        if (files.Length > 0){
                            string path = files[0].Substring(Application.dataPath.Length + 11);
                            path = path.Substring(0, path.Length - 7);
                            obj = (GameObject)Resources.Load(path, typeof(GameObject));
                        }
                    }
                }
            }
            if(obj != null)
            {
                MonoBehaviour.Instantiate<GameObject>(obj, position, Quaternion.Euler(rotation), null);
                DebugCLI.OnMessage(GetColoredText("spawn", TextColorType.Command) + ": '" + GetColoredText(name, TextColorType.Parameter) + "' spawned at position " + GetColoredText(position.ToString(), TextColorType.Parameter) + ", rotation " + GetColoredText(rotation.ToString(), TextColorType.Parameter) + "!", MessageType.Affirmation);
            } else
            {
                DebugCLI.OnMessage(GetColoredText("spawn", TextColorType.Command) + ": Could not find '" + GetColoredText(name, TextColorType.Parameter) + "'!", MessageType.Notification);
            }
        }

        [ConsoleCommand("scene", description = "Changes the Unity Scene")]
        public static void ChangeScene(string identifier)
        {
            int sceneIndex = -1;
            if (int.TryParse(identifier, out sceneIndex))
            {
                UnityEngine.SceneManagement.SceneManager.LoadScene(sceneIndex, UnityEngine.SceneManagement.LoadSceneMode.Single);
            } else
            {
                UnityEngine.SceneManagement.SceneManager.LoadScene(identifier, UnityEngine.SceneManagement.LoadSceneMode.Single);
            }
        }

        [ConsoleCommand("search", description = "Searches the scene for given GameObject and targets it")]
        public void Search(string name)
        {
            object obj = (object)GameObject.Find(name);
            if(obj != null)
            {
                _target = obj;
                DebugCLI.OnMessage(GetColoredText("search", TextColorType.Command) + ": Found " + GetColoredText(name, TextColorType.Parameter), MessageType.Affirmation);
                return;
            }
            DebugCLI.OnMessage(GetColoredText("search", TextColorType.Command) + ": Could not find any objects named '" + GetColoredText(name, TextColorType.Parameter) + "'", MessageType.Notification);
        }

        [ConsoleCommand("search", funcName = "type", description = "Searches the scene for given type of an object and targets it")]
        public void SearchType(string typeName)
        {
            object obj = null;
            System.Type type = System.Type.GetType(name, false, true);
            if (type != null)
            {
                obj = FindObjectOfType(type);
                if (obj != null)
                {
                    _target = obj;
                    DebugCLI.OnMessage(GetColoredText("search", TextColorType.Command) + ": Found " + GetColoredText(type.ToString(), TextColorType.Parameter), MessageType.Affirmation);
                }
                else
                {
                    DebugCLI.OnMessage(GetColoredText("search", TextColorType.Command) + ": Could not find any objects of type '" + GetColoredText(name, TextColorType.Parameter) + "'", MessageType.Notification);
                }
            }
            else
            {
                DebugCLI.OnMessage(GetColoredText("search", TextColorType.Command) + ": '" + GetColoredText(name, TextColorType.Parameter) + "' is not a known type!", MessageType.Warning);
            }
        }
        #endregion
    }
}
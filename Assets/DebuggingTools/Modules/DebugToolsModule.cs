﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DebuggingTools
{
    public abstract class DebugToolsModule : MonoBehaviour
    {

        public virtual KeyCode ToggleKey { get { return KeyCode.None; } }
        protected bool _enabled = false;
        public bool Enabled { get { return _enabled; } }
        protected bool _show = false;
        public bool Show { get { return _show; } }

        public virtual void Enable()
        {
            _enabled = true;
        }

        public virtual void Disable()
        {
            _enabled = false;
        }

        public virtual void Toggle()
        {
            _show = !_show;
        }

        public virtual void Update()
        {

        }

        public virtual void DrawGUI()
        {

        }
    }
}